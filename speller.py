#!/usr/bin/env python3

# ---------------------------------------------------------------------
# Video-audio speller, main file
# Copyleft (C) 2022, Univ. Lille
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------

# ---------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------

import wx
import wx.adv
import platform
import symbols  # Speller tree of symbols
from typing import Tuple
from pubsub import pub


# Checks whether the current platform is a Raspberry Pi or not.
def is_raspberry_pi() -> bool:
    return platform.machine() in ('armv7l', 'armv6l')


if is_raspberry_pi():
    import RPi.GPIO as GPIO
    pin = 40  # Pin number of the selection switch

# ---------------------------------------------------------------------
# Global functions
# ---------------------------------------------------------------------


# Clears activation and selection flags from a tree of symbols
def deactivate(symbols: dict):
    while len(symbols['child']) != 0:
        state, n = 0, len(symbols['child'])
        for pos in range(n):
            if symbols['child'][pos]['state']:
                state = symbols['child'][pos]['state']
                break
        if state:
            symbols['child'][pos]['state'] = 0
            symbols = symbols['child'][pos]
        else:
            break


# Determines the grid size required to display a tree of symbols
# returns the numbers of required columns and rows
def grid(symbols: dict) -> Tuple[int, int]:
    gcols, grows = len(symbols['child']), -1
    for branch in symbols['child']:
        bcols, brows = grid(branch)
        gcols = max(gcols, bcols)
        grows = max(grows, brows)
    return gcols, grows + 1


# Outputs the selected symbol. Either prints its associated string
# to stdout, on testing platforms, or sends scancodes on the gadget
# keyboard on the Raspberry Pi Zero platform.
def send(symbol: dict):
    if is_raspberry_pi():
        with open('/dev/hidg0', 'wb+') as fd:
            for i in range(0, len(symbol['scancodes']), 2):
                scancodes = symbol['scancodes'][i] + chr(
                    0) + symbol['scancodes'][i + 1] + 13 * chr(0)
                fd.write(scancodes.encode())
    else:
        print(symbol['output'])
        with open('output.bin', 'ab+') as fd:
            for i in range(0, len(symbol['scancodes']), 2):
                scancodes = symbol['scancodes'][i] + chr(
                    0) + symbol['scancodes'][i + 1] + 13 * chr(0)
                fd.write(scancodes.encode())


# ---------------------------------------------------------------------
# Classes
# ---------------------------------------------------------------------


class Speller(wx.Frame):

    def __init__(self, *args, **kw):
        super(Speller, self).__init__(*args, **kw)
        self.symbols = dict(symbols.symbols)
        self.grid = grid(self.symbols)
        self.InitUI()
        self.InitTimer()
        self.panel = wx.Panel(self, wx.ID_ANY)
        self.panel.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)
        self.panel.SetFocus()
        self.running = False
        #        self.ShowFullScreen(True)
        self.Maximize(True)
        wx.LogStderr.AddTraceMask('sound')
        pub.subscribe(self.Listener, "listener")

    def InitUI(self):
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.SetTitle(
            'Speller (' +
            wx.PlatformInformation.Get().GetOperatingSystemDescription() + ')')
        self.Centre()

    def InitTimer(self):
        self.timer = wx.Timer(self)
        """Timer for speller evolution"""
        self.Bind(wx.EVT_TIMER, self.OnTimer, self.timer)

    def OnPaint(self, event):
        dc = wx.PaintDC(self)
        size = self.GetClientSize()
        y, s = 0, min(size[0] // self.grid[0], size[1] // self.grid[1])
        font = wx.Font(s // 2, wx.ROMAN, wx.NORMAL, wx.BOLD)
        dc.SetFont(font)
        row = self.symbols
        while len(row['child']) != 0:
            next, n, pos = False, 0, 0
            for symbol in row['child']:
                if symbol['state']:
                    dc.SetBrush(
                        wx.Brush(row['color']['ab'] if symbol['state'] ==
                                 1 else row['color']['sb']))
                    next, pos = True, n
                else:
                    dc.SetBrush(wx.Brush(row['color']['b']))
                dc.DrawRectangle(s * n, y, s, s)
                tx, ty = dc.GetTextExtent(symbol['str'])
                dc.DrawText(symbol['str'], s * n + int((s - tx) / 2),
                            int(y + (s - ty) / 2))
                n += 1
            if next:
                y += s
                row = row['child'][pos]
            else:
                break

    def OnTimer(self, timer):
        row = self.symbols
        while len(row['child']) != 0:
            state, n = 0, len(row['child'])
            for pos in range(n):
                if row['child'][pos]['state']:
                    state = row['child'][pos]['state']
                    break
            if state == 2:
                row = row['child'][pos]
            elif state == 1:
                row['child'][pos]['state'] = 0
                if pos == n - 1:
                    next = 0
                    self.cycle += 1
                    if self.cycle == 2:
                        self.timer.Stop()
                        self.running = False
                        deactivate(self.symbols)
                        break
                else:
                    next = pos + 1
                row['child'][next]['state'] = 1
                self.sound = wx.adv.Sound(row['child'][next]['sound'])
                if self.sound.IsOk():
                    self.sound.Play(wx.adv.SOUND_ASYNC)
                break
            else:
                row['child'][0]['state'] = 1
                self.sound = wx.adv.Sound(row['child'][0]['sound'])
                if self.sound.IsOk():
                    self.sound.Play(wx.adv.SOUND_ASYNC)
                break
        self.Refresh()

    def OnKeyDown(self, event):
        keycode = event.GetKeyCode()
        event.Skip()
        wx.CallAfter(self.Clicked)

    def Listener(self, message):
        if message == "falling":
            wx.CallAtfer(self.Clicked)

    def Clicked(self):
        if not self.running:
            self.running = True
            self.cycle = 0
            self.timer.Start(2000)
            self.OnTimer(self)
        else:
            row = self.symbols
            while len(row['child']) != 0:
                state, n = 0, len(row['child'])
                for pos in range(n):
                    if row['child'][pos]['state']:
                        state = row['child'][pos]['state']
                        break
                if state == 2:
                    row = row['child'][pos]
                elif state == 1:
                    row['child'][pos]['state'] = 2
                    self.sound = wx.adv.Sound(row['child'][pos]['sound'])
                    if self.sound.IsOk():
                        self.sound.Play(wx.adv.SOUND_ASYNC)
                    if len(row['child'][pos]['child']) == 0:
                        send(row['child'][pos])
                        deactivate(self.symbols)
                    break
                else:
                    break
            self.Refresh()


def falling(channel):
    if not GPIO.input(pin):
        pub.sendMessage("listener", message="falling")


def main():
    if is_raspberry_pi():
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.add_event_detect(pin,
                              GPIO.FALLING,
                              callback=falling,
                              bouncetime=200)
    app = wx.App()
    sp = Speller(None)
    sp.Show()
    app.MainLoop()


if __name__ == '__main__':
    main()
