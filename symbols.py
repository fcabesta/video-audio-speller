# ---------------------------------------------------------------------
# Video-audio speller, tree of symbols
# Copyleft (C) 2022, Univ. Lille
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------

symbols = {
    'color': {
        'b': '#FFFFFF',
        't': '#000000',
        'ab': '#FFFF00',
        'at': '#000000',
        'sb': '#FF0000',
        'st': '#000000'
    },
    'child': [{
        'str':
        '1',
        'sound':
        'sounds/1.wav',
        'state':
        0,
        'color': {
            'b': '#FFFFFF',
            't': '#000000',
            'ab': '#FFFF00',
            'at': '#000000',
            'sb': '#FF0000',
            'st': '#000000'
        },
        'child': [{
            'str': 'A',
            'sound': 'sounds/a.wav',
            'state': 0,
            'output': 'A',
            'scancodes': '\x02\x04',
            'child': []
        }, {
            'str': 'E',
            'sound': 'sounds/e.wav',
            'state': 0,
            'output': 'E',
            'scancodes': '\x02\x08',
            'child': []
        }, {
            'str': 'I',
            'sound': 'sounds/i.wav',
            'state': 0,
            'output': 'I',
            'scancodes': '\x02\x0c',
            'child': []
        }, {
            'str': 'O',
            'sound': 'sounds/o.wav',
            'state': 0,
            'output': 'O',
            'scancodes': '\x02\x12',
            'child': []
        }, {
            'str': 'U',
            'sound': 'sounds/u.wav',
            'state': 0,
            'output': 'U',
            'scancodes': '\x02\x18',
            'child': []
        }, {
            'str': 'Y',
            'sound': 'sounds/y.wav',
            'state': 0,
            'output': 'Y',
            'scancodes': '\x02\x1c',
            'child': []
        }]
    }, {
        'str':
        '2',
        'sound':
        'sounds/2.wav',
        'state':
        0,
        'color': {
            'b': '#FFFFFF',
            't': '#000000',
            'ab': '#FFFF00',
            'at': '#000000',
            'sb': '#FF0000',
            'st': '#000000'
        },
        'child': [{
            'str': 'B',
            'sound': 'sounds/b.wav',
            'state': 0,
            'output': 'B',
            'scancodes': '\x02\x05',
            'child': []
        }, {
            'str': 'C',
            'sound': 'sounds/c.wav',
            'state': 0,
            'output': 'C',
            'scancodes': '\x02\x06',
            'child': []
        }, {
            'str': 'D',
            'sound': 'sounds/d.wav',
            'state': 0,
            'output': 'D',
            'scancodes': '\x02\x07',
            'child': []
        }, {
            'str': 'F',
            'sound': 'sounds/f.wav',
            'state': 0,
            'output': 'F',
            'scancodes': '\x02\x09',
            'child': []
        }, {
            'str': 'G',
            'sound': 'sounds/g.wav',
            'state': 0,
            'output': 'G',
            'scancodes': '\x02\x0a',
            'child': []
        }]
    }, {
        'str':
        '3',
        'sound':
        'sounds/3.wav',
        'state':
        0,
        'color': {
            'b': '#FFFFFF',
            't': '#000000',
            'ab': '#FFFF00',
            'at': '#000000',
            'sb': '#FF0000',
            'st': '#000000'
        },
        'child': [{
            'str': 'H',
            'sound': 'sounds/h.wav',
            'state': 0,
            'output': 'H',
            'scancodes': '\x02\x0b',
            'child': []
        }, {
            'str': 'J',
            'sound': 'sounds/j.wav',
            'state': 0,
            'output': 'J',
            'scancodes': '\x02\x0d',
            'child': []
        }, {
            'str': 'K',
            'sound': 'sounds/k.wav',
            'state': 0,
            'output': 'K',
            'scancodes': '\x02\x0e',
            'child': []
        }, {
            'str': 'L',
            'sound': 'sounds/l.wav',
            'state': 0,
            'output': 'L',
            'scancodes': '\x02\x0f',
            'child': []
        }, {
            'str': 'M',
            'sound': 'sounds/m.wav',
            'state': 0,
            'output': 'M',
            'scancodes': '\x02\x10',
            'child': []
        }]
    }, {
        'str':
        '4',
        'sound':
        'sounds/4.wav',
        'state':
        0,
        'color': {
            'b': '#FFFFFF',
            't': '#000000',
            'ab': '#FFFF00',
            'at': '#000000',
            'sb': '#FF0000',
            'st': '#000000'
        },
        'child': [{
            'str': 'N',
            'sound': 'sounds/n.wav',
            'state': 0,
            'output': 'N',
            'scancodes': '\x02\x11',
            'child': []
        }, {
            'str': 'P',
            'sound': 'sounds/p.wav',
            'state': 0,
            'output': 'P',
            'scancodes': '\x02\x13',
            'child': []
        }, {
            'str': 'Q',
            'sound': 'sounds/q.wav',
            'state': 0,
            'output': 'Q',
            'scancodes': '\x02\x14',
            'child': []
        }, {
            'str': 'R',
            'sound': 'sounds/r.wav',
            'state': 0,
            'output': 'R',
            'scancodes': '\x02\x15',
            'child': []
        }, {
            'str': 'S',
            'sound': 'sounds/s.wav',
            'state': 0,
            'output': 'S',
            'scancodes': '\x02\x16',
            'child': []
        }]
    }, {
        'str':
        '5',
        'sound':
        'sounds/5.wav',
        'state':
        0,
        'color': {
            'b': '#FFFFFF',
            't': '#000000',
            'ab': '#FFFF00',
            'at': '#000000',
            'sb': '#FF0000',
            'st': '#000000'
        },
        'child': [{
            'str': 'T',
            'sound': 'sounds/t.wav',
            'state': 0,
            'output': 'T',
            'scancodes': '\x02\x17',
            'child': []
        }, {
            'str': 'V',
            'sound': 'sounds/v.wav',
            'state': 0,
            'output': 'V',
            'scancodes': '\x02\x19',
            'child': []
        }, {
            'str': 'W',
            'sound': 'sounds/w.wav',
            'state': 0,
            'output': 'W',
            'scancodes': '\x02\x1a',
            'child': []
        }, {
            'str': 'X',
            'sound': 'sounds/x.wav',
            'state': 0,
            'output': 'X',
            'scancodes': '\x02\x1b',
            'child': []
        }, {
            'str': 'Z',
            'sound': 'sounds/z.wav',
            'state': 0,
            'output': 'Z',
            'scancodes': '\x02\x1d',
            'child': []
        }]
    }, {
        'str': '6',
        'sound': 'sounds/6.wav',
        'state': 0,
        'output': 'Aspiration bouche',
        'scancodes': '\x00\x3a', # F1
        'child': []
    }, {
        'str': '7',
        'sound': 'sounds/7.wav',
        'state': 0,
        'output': 'Aspiration trac',
        'scancodes': '\x00\x3b', # F2
        'child': []
    }, {
        'str': '8',
        'sound': 'sounds/8.wav',
        'state': 0,
        'output': 'Erreur',
        'scancodes': '\x00\x2a', # Backspace
        'child': []
    }, {
        'str': '9',
        'sound': 'sounds/9.wav',
        'state': 0,
        'output': 'Aller au lit',
        'scancodes': '\x00\x3c', # F3
        'child': []
    }, {
        'str': '0',
        'sound': 'sounds/0.wav',
        'state': 0,
        'output': 'Cof assist',
        'scancodes': '\x00\x3d', # F4
        'child': []
    }]
}
